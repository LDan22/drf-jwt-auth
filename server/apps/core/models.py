from django.db import models


class TimestampedModel(models.Model):
    """An abstract behavior representing timestamping a model."""

    created = models.DateTimeField(auto_now_add=True, db_index=True)
    modified = models.DateTimeField(auto_now=True, db_index=True)

    class Meta(object):
        abstract = True
