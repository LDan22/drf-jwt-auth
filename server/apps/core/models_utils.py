from rest_framework.utils import model_meta


def get_to_many_model_fields(model_class):
    """Get m2m fields names of model."""
    model_relations = _get_model_relations(model_class)
    return [
        field_name
        for field_name, relation_info in model_relations.items()
        if relation_info.to_many
    ]


def _get_model_relations(model_class):
    """Get all model relations."""
    return model_meta.get_field_info(model_class).relations


def set_fields_values(entity, fields_values):
    if fields_values:
        for field_name, field_value in fields_values.items():
            setattr(entity, field_name, field_value)
    return entity


def set_to_many_fields_values(entity, fields_values):
    if fields_values:
        for field_name, field_value in fields_values.items():
            field = getattr(entity, field_name)
            field.set(field_value)
    return entity
