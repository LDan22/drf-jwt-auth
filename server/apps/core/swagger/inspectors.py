from collections import OrderedDict

import coreschema
from django.conf import settings
from drf_yasg import openapi
from drf_yasg.inspectors import PaginatorInspector
from drf_yasg.utils import force_real_str


class ApiLimitOffsetPaginatorInspector(PaginatorInspector):  # pragma: no cover
    """
    Paginator inspector for swagger.

    Based on server.apps.core.pagination.ApiLimitOffsetPagination.
    """

    def get_paginator_parameters(self, paginator):
        try:
            fields = paginator.get_schema_fields(self.view)
        except AttributeError:
            fields = []
        return [core_api_field_to_parameter(field) for field in fields]

    def get_paginated_response(self, paginator, response_schema):
        return openapi.Schema(
            type=openapi.TYPE_OBJECT,
            properties=OrderedDict((
                ('results', response_schema),
                ('meta', openapi.Schema(
                    type=openapi.TYPE_OBJECT,
                    properties=OrderedDict((
                        ('pagination', openapi.Schema(
                            type=openapi.TYPE_OBJECT,
                            properties=OrderedDict((
                                (
                                    'count',
                                    openapi.Schema(
                                        type=openapi.TYPE_INTEGER,
                                        description='Total results.',
                                    ),
                                ),
                                (
                                    'limit',
                                    openapi.Schema(
                                        type=openapi.TYPE_INTEGER,
                                        default=settings.PAGE_SIZE,
                                    ),
                                ),
                                (
                                    'offset',
                                    openapi.Schema(
                                        type=openapi.TYPE_INTEGER,
                                        default=0,
                                    ),
                                ),
                            )),
                        )),
                    )),
                )),
                ('links', openapi.Schema(
                    type=openapi.TYPE_OBJECT,
                    properties=OrderedDict((
                        (
                            'first',
                            openapi.Schema(
                                type=openapi.TYPE_STRING,
                                format=openapi.FORMAT_URI,
                            ),
                        ),
                        (
                            'last',
                            openapi.Schema(
                                type=openapi.TYPE_STRING,
                                format=openapi.FORMAT_URI,
                            ),
                        ),
                        (
                            'next',
                            openapi.Schema(
                                type=openapi.TYPE_STRING,
                                format=openapi.FORMAT_URI,
                                x_nullable=True,
                            ),
                        ),
                        (
                            'previous',
                            openapi.Schema(
                                type=openapi.TYPE_STRING,
                                format=openapi.FORMAT_URI,
                                x_nullable=True,
                            ),
                        ),
                    )),
                )),
            )),
            required=['results'],
        )


def core_api_field_to_parameter(field):  # pragma: no cover
    """Convert an instance of `coreapi.Field` to a swagger Parameter object."""
    location_to_in = {
        'query': openapi.IN_QUERY,
        'path': openapi.IN_PATH,
        'form': openapi.IN_FORM,
        'body': openapi.IN_FORM,
    }
    core_api_types = {
        coreschema.Integer: openapi.TYPE_INTEGER,
        coreschema.Number: openapi.TYPE_NUMBER,
        coreschema.String: openapi.TYPE_STRING,
        coreschema.Boolean: openapi.TYPE_BOOLEAN,
    }

    core_schema_attrs = [
        'format', 'pattern', 'enum', 'min_length', 'max_length',
    ]
    schema = field.schema
    return openapi.Parameter(
        name=field.name,
        in_=location_to_in[field.location],
        required=field.required,
        description=force_real_str(schema.description) if schema else None,
        type=core_api_types.get(type(schema), openapi.TYPE_STRING),
        **OrderedDict(
            (attr, getattr(schema, attr, None)) for attr in core_schema_attrs
        ),
    )
