from django.contrib.auth import get_user_model
from drf_yasg.utils import swagger_serializer_method
from rest_framework import serializers

from server.apps.users.models import UserProfile


class UserSerializer(serializers.ModelSerializer):
    """User output serializer."""

    class Meta(object):
        model = get_user_model()
        fields = ('id', 'email', 'is_active', 'is_staff')


class ProfileSerializer(serializers.ModelSerializer):
    """User profile public output serializer."""

    class Meta(object):
        model = UserProfile
        fields = ('id', 'bio', 'email_verified', 'profile_pic')


class UserProfileSerializer(serializers.ModelSerializer):
    """
    Composed serializer from `User` and `UserProfile`.

    Used for serializing response about user.
    """

    profile = serializers.SerializerMethodField('serializer_user_profile')

    class Meta(object):
        model = get_user_model()
        fields = (
            'id',
            'email',
            'is_active',
            'is_staff',
            'is_superuser',
            'profile',
        )

    @swagger_serializer_method(serializer_or_field=ProfileSerializer)
    def serializer_user_profile(self, user):
        request = self.context.get('request')
        profile = user.profile
        profile_serializer = ProfileSerializer(
            instance=profile,
            many=False,
            context={'request': request},
        )
        return profile_serializer.data
