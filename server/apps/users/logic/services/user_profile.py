from server.apps.core.services import ModelService
from server.apps.users.models import UserProfile


class UserProfileService(ModelService):
    """Service for `UserProfile` model."""

    model_class = UserProfile

    @staticmethod
    def set_email_verified(user_profile):
        user_profile.email_verified = True
        user_profile.save()
        return user_profile
