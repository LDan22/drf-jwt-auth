from django.core.exceptions import ObjectDoesNotExist

from server.apps.core.services import ModelService
from server.apps.users.logic.selectors import UserTokenSelector
from server.apps.users.models import UserToken, UserTokenType


class UserTokenService(ModelService):
    """Service for `UserToken` model."""

    model_class = UserToken

    def __init__(self):
        super().__init__()
        self.user_token_selector = UserTokenSelector()

    def create(self, user, token_type, **kwargs):
        """
        Create token for user.

        First we clean tokens for user with same type, because user cannot have
        more than one token with same type.
        """
        self.delete_for_user_by_type(user, token_type)
        return super().create(user=user, type=token_type, **kwargs)

    def create_password_recover_token(self, user):
        return self.create(user, UserTokenType.PASSWORD_RECOVER)

    def create_account_activation_token(self, user):
        return self.create(user, UserTokenType.VERIFICATION)

    def delete_for_user_by_type(self, user, token_type):
        """
        Deletes token by its user and type.

        Because of unique constraint (user, type) we are sure that there can be
        only one such instance and we use `UserTokenSelector().get_one_by()`
        instead of `UserTokenSelector().filter_by()`.
        """
        try:
            return self.user_token_selector.get_one_by(
                user=user,
                type=token_type,
            ).delete()
        except ObjectDoesNotExist:
            return None
