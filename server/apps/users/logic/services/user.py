from django.contrib.auth import get_user_model

from server.apps.core.services import ModelService
from server.apps.users.logic.services.user_profile import UserProfileService


class UserService(ModelService):
    """Service for user model."""

    model_class = get_user_model()

    def __init__(self):
        super().__init__()
        self.profile_service = UserProfileService()

    def create(self, email, password, **extra_fields):
        """
        User model requires special logic for creating new user.

        Password should be stored hashed, therefore we use method from
        UserManager for creating new user.
        Create user's profile after new user is created.
        """
        new_user = self.model_class.objects.create_user(
            email=email,
            password=password,
            **extra_fields,
        )
        self.profile_service.create(user=new_user)
        return new_user

    @staticmethod
    def set_password(user, new_password):
        user.set_password(new_password)
        user.save()
