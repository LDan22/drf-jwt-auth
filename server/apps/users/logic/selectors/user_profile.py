from server.apps.core.selectors import ModelSelector
from server.apps.users.models import UserProfile


class UserProfileSelector(ModelSelector):
    """Selector for `UserProfile` model."""

    model_class = UserProfile
