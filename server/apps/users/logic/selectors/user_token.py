from rest_framework.exceptions import ValidationError

from server.apps.core.selectors import ModelSelector
from server.apps.users.models import UserToken


class UserTokenSelector(ModelSelector):
    """Selector for `UserToken` model."""

    model_class = UserToken

    def get_one_by(self, **fields):
        user_token = super().get_one_by(**fields)
        if user_token.is_expired:
            raise ValidationError('Token is expired')

        return user_token
