import django_filters
from django.contrib.auth import get_user_model


class UserFilter(django_filters.FilterSet):
    """User filterset class."""

    class Meta(object):
        model = get_user_model()
        fields = {
            'email': ['exact', 'contains'],
        }
        exclude = ['password']
