from django.conf import settings
from django.db import migrations, models

from server.apps.users.models.user_profile import get_profile_pic_upload_path


class Migration(migrations.Migration):
    """`UserProfile` initial migration file."""

    dependencies = [
        ('users', '0002_user_token'),
    ]

    operations = [
        migrations.CreateModel(
            name='UserProfile',
            fields=[
                (
                    'id',
                    models.BigAutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name='ID',
                    ),
                ),
                ('bio', models.TextField()),
                ('email_verified', models.BooleanField(default=False)),
                (
                    'profile_pic',
                    models.ImageField(
                        blank=True,
                        default='default_profile_pic.png',
                        null=True,
                        upload_to=get_profile_pic_upload_path,
                    ),
                ),
                (
                    'user',
                    models.OneToOneField(
                        on_delete=models.deletion.CASCADE,
                        to=settings.AUTH_USER_MODEL,
                    ),
                ),
            ],
        ),
    ]
