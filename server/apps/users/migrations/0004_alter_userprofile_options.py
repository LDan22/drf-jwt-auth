from django.db import migrations


class Migration(migrations.Migration):
    """Add verbose names to meta class for `UserProfile`."""

    dependencies = [
        ('users', '0003_userprofile'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='userprofile',
            options={
                'verbose_name': 'User profile',
                'verbose_name_plural': 'User profiles',
            },
        ),
    ]
