import uuid

from django.conf import settings
from django.db import migrations, models

import server.apps.users.models.user_token


class Migration(migrations.Migration):
    """Migration file for creating `UserToken` model."""

    dependencies = [
        ('users', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='UserToken',
            fields=[
                (
                    'id',
                    models.BigAutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name='ID',
                    ),
                ),
                (
                    'key',
                    models.UUIDField(
                        default=uuid.uuid4,
                    ),
                ),
                (
                    'type',
                    models.CharField(
                        blank=True,
                        choices=[
                            ('VER', 'Verification'),
                            ('PSR', 'Password recover'),
                        ],
                        max_length=3,
                    ),
                ),
                (
                    'expires',
                    models.DateTimeField(
                        default=server.apps.users.models.user_token.get_token_expiration_date,  # noqa: E501, WPS219
                    ),
                ),
                (
                    'user',
                    models.ForeignKey(
                        on_delete=models.deletion.CASCADE,
                        to=settings.AUTH_USER_MODEL,
                    ),
                ),
            ],
            options={
                'verbose_name': 'User token',
                'verbose_name_plural': 'User tokens',
            },
        ),
        migrations.AddConstraint(
            model_name='usertoken',
            constraint=models.CheckConstraint(
                check=models.Q(('type__in', ['VER', 'PSR', ''])),
                name='auth_usertoken_type_check',
            ),
        ),
        migrations.AddConstraint(
            model_name='usertoken',
            constraint=models.UniqueConstraint(
                fields=('user', 'type'),
                name='auth_usertoken_user_type_key',
            ),
        ),
    ]
