from django.contrib.auth import get_user_model
from django.db import models


def get_profile_pic_upload_path(instance, filename):
    return '/'.join(['profile_pics', instance.user.email, filename])


class UserProfile(models.Model):
    """
    User profile model.

    Contains additional information for a user.
    """

    user = models.OneToOneField(
        get_user_model(),
        related_name='profile',
        on_delete=models.CASCADE,
    )
    bio = models.TextField()
    email_verified = models.BooleanField(default=False)
    profile_pic = models.ImageField(
        blank=True,
        null=True,
        default='default_profile_pic.png',
        upload_to=get_profile_pic_upload_path,
    )

    class Meta(object):
        verbose_name = 'User profile'
        verbose_name_plural = 'User profiles'

    def __str__(self):
        """Represent profile by user."""
        return str(self.user)
