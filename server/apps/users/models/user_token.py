from uuid import uuid4

from django.conf import settings
from django.contrib.auth import get_user_model
from django.db import models
from django.utils import timezone
from django.utils.translation import gettext_lazy as _


def get_token_expiration_date():
    """Return user token expiration time."""
    return timezone.now() + timezone.timedelta(
        minutes=settings.USER_TOKEN_LIFETIME,
    )


class UserTokenType(models.TextChoices):
    """Define types of user tokens."""

    VERIFICATION = 'VER', _('Verification')
    PASSWORD_RECOVER = 'PSR', _('Password recover')


class UserToken(models.Model):
    """
    Token for a user.

    Used for account activation, password recovering.Should be deleted
    after used. Unused tokens should be cleaned up be async workers after
    expiration.

    User can have only one type of token in database. Before creating
    new token, delete other of same type.
    """

    user = models.ForeignKey(
        get_user_model(),
        on_delete=models.CASCADE,
        db_index=True,
    )
    key = models.UUIDField(default=uuid4)
    type = models.CharField(
        max_length=3,
        choices=UserTokenType.choices,
        blank=True,
    )
    expires = models.DateTimeField(default=get_token_expiration_date)

    class Meta(object):
        verbose_name = 'User token'
        verbose_name_plural = 'User tokens'

        constraints = [
            models.CheckConstraint(
                name='auth_usertoken_type_check',
                check=models.Q(type__in=UserTokenType.values + ['']),
            ),
            models.UniqueConstraint(
                name='auth_usertoken_user_type_key',
                fields=['user', 'type'],
            ),
        ]

    def __str__(self):
        """Represent token by its key."""
        return str(self.key)

    @property
    def is_expired(self):
        """Check if token is expired."""
        return timezone.now() > self.expires
