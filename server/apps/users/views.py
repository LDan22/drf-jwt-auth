from rest_framework.generics import ListAPIView

from server.apps.users.logic.filters import UserFilter
from server.apps.users.logic.selectors import UserSelector
from server.apps.users.logic.serializers import UserSerializer


class UsersListAPIView(ListAPIView):
    """List all active users."""

    serializer_class = UserSerializer
    filterset_class = UserFilter

    def get_queryset(self):
        return UserSelector().get_all_active()
