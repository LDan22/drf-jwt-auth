from django.conf import settings

from server.apps.mailings.logic.clients import SendGridClient
from server.apps.mailings.logic.mails import ActivationMail, PasswordResetMail


class BaseMailer(object):
    """
    Mailer used by service code to send mails.

    Adapter for mailings client and mail to send.
    """

    client_class = None

    def __init__(self, to_emails, mail):
        self.to_emails = to_emails
        self.mail = mail

    def get_client(self):
        """Client object used to send emails."""
        if self.client_class is None:
            raise ValueError('Client class can not be None.')

        return self.client_class(self.to_emails, self.mail)

    def send(self):
        """Sending emails method."""
        return self.get_client().send()


class SendGridMailer(BaseMailer):
    """
    SendGrid mailer.

    API for client code.
    """

    client_class = SendGridClient


class SendgridActivationMailer(SendGridMailer):
    """Mailer for account activation."""

    def __init__(self, user, activation_token):
        self.activation_token = activation_token
        activation_url = self._build_activation_url()
        super().__init__([user.email], ActivationMail(user, activation_url))

    def _build_activation_url(self):
        return '{0}/{1}'.format(
            settings.API_URL,
            self.activation_token,
        )


class SendgridPasswordRecoverMailer(SendGridMailer):
    """Mailer for password resetting."""

    def __init__(self, email, password_reset_token):
        self.password_reset_token = password_reset_token
        password_reset_url = self._build_password_reset_url()
        super().__init__([email], PasswordResetMail(password_reset_url))

    def _build_password_reset_url(self):
        return '{0}/{1}'.format(
            settings.CLIENT_URL,
            self.password_reset_token,
        )
