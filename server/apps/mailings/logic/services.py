from server.apps.mailings.logic.mailers import (
    SendgridActivationMailer,
    SendgridPasswordRecoverMailer,
)
from server.apps.users.logic.services.user_token import UserTokenService


class MailingsService(object):
    """Services for sending emails."""

    def __init__(self):
        self.user_token_service = UserTokenService()

    def send_activation_mail(self, user):
        user_token = self.user_token_service.create_account_activation_token(
            user=user,
        )
        SendgridActivationMailer(user, user_token.key).send()

    def send_password_recover_mail(self, user):
        user_token = self.user_token_service.create_password_recover_token(
            user=user,
        )
        SendgridPasswordRecoverMailer(user.email, user_token.key).send()
