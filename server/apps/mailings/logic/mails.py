from django.template.loader import render_to_string


class BaseMail(object):
    """Represents a simple mail with subject and html content."""

    def __init__(self, subject, html_content=None):
        self.subject = subject
        self.html_content = html_content


class ActivationMail(BaseMail):
    """Mail for account activation."""

    def __init__(self, user, activation_url):
        self.user = user
        self.activation_url = activation_url
        super().__init__(
            subject='Account activation',
            html_content=self._render_activation_mail(),
        )

    def _render_activation_mail(self):
        """Return a rendered activation mail."""
        return render_to_string('mails/activation_mail.html', {
            'email': self.user.email,
            'activation_url': self.activation_url,
        })


class PasswordResetMail(BaseMail):
    """Password reset mail."""

    def __init__(self, password_reset_url):
        self.password_reset_url = password_reset_url
        super().__init__(
            subject='Password reset',
            html_content=self._render_activation_mail(),
        )

    def _render_activation_mail(self):
        """Return a rendered activation mail."""
        return render_to_string('mails/password_reset.html', {
            'password_reset_url': self.password_reset_url,
        })
