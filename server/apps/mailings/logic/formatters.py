from abc import ABC, abstractmethod

from django.conf import settings
from sendgrid import Mail as SendGridMail

from server.apps.mailings.logic.mails import BaseMail


class MailFormatter(ABC):
    """
    Mail builder to a specific format.

    Many providers can have same format of mails, so we use a separate class
    to format mails.
    """

    @abstractmethod
    def format_mail(self, to_emails, mail: BaseMail):
        """Format base mail to format required by provider."""


class SendGridMailFormatter(MailFormatter):
    """Mail formatted class for SendGrid provider."""

    def format_mail(self, to_emails, mail):
        return SendGridMail(
            from_email=settings.SENDGRID_FROM_EMAIL,
            to_emails=to_emails,
            subject=mail.subject,
            html_content=mail.html_content,
        )
