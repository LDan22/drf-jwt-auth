from server.apps.mailings.logic.formatters import SendGridMailFormatter
from server.apps.mailings.logic.providers import SendGridProvider


class MailingsClient(object):
    """
    Mailings client used by mailer to send mails.

    Encapsulates information about formatting and provider requirements.
    """

    provider_class = None
    formatter_class = None

    def __init__(self, to_emails, mail):
        self.to_emails = to_emails
        self.mail = mail

    def get_provider(self):
        """Provider object used to send mails."""
        if self.provider_class is None:
            raise ValueError('Provider class can not be None.')
        return self.provider_class()

    def get_formatter(self):
        """Mail formatter object used to format mail for provider."""
        if self.formatter_class is None:
            raise ValueError('Formatter class can not be None.')
        return self.formatter_class()

    def get_formatted_mail(self):
        """Formatted mail used by provider."""
        return self.get_formatter().format_mail(self.to_emails, self.mail)

    def send(self):
        """Method used by client code to send emails."""
        return self.get_provider().send_mail(self.get_formatted_mail())


class SendGridClient(MailingsClient):
    """SendGrid mailings client."""

    provider_class = SendGridProvider
    formatter_class = SendGridMailFormatter
