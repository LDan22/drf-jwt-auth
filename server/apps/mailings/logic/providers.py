from abc import ABC, abstractmethod

from django.conf import settings
from sendgrid import SendGridAPIClient


class MailingsProvider(ABC):
    """Third party provider for mailings."""

    @abstractmethod
    def send_mail(self, formatted_mail):
        """
        Method for sending email.

        Args:
            formatted_mail (): Email in format which provider requires.
        """


class SendGridProvider(MailingsProvider):
    """
    Sendgrid provider.

    Main role is being a wrapper of third party SendGridAPIClient.
    """

    def __init__(self):
        self.sg = SendGridAPIClient(settings.SENDGRID_API_KEY)

    def send_mail(self, formatted_mail):
        """Send email using sendgrid api."""
        return self.sg.send(formatted_mail)
