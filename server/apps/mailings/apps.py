from django.apps import AppConfig


class MailingsConfig(AppConfig):
    """Mailings app configuration."""

    default_auto_field = 'django.db.models.BigAutoField'
    name = 'server.apps.mailings'
