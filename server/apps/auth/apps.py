from django.apps import AppConfig


class AuthConfig(AppConfig):
    """Auth app configuration."""

    default_auto_field = 'django.db.models.BigAutoField'
    name = 'server.apps.auth'
