from django.urls import include, path

from server.apps.auth.views import (
    DecoratedTokenObtainPairView,
    DecoratedTokenRefreshView,
    DecoratedTokenVerifyView,
    LogoutAPIView,
    RegistrationAPIView,
    UserProfileActivation,
)
from server.apps.auth.views.auth import CurrentUserAPIView
from server.apps.auth.views.password import (
    PasswordChangeAPIView,
    PasswordRecoverRequestAPIView,
    PasswordResetAPIView,
)

app_name = 'auth'

urlpatterns = [
    path('register/', RegistrationAPIView.as_view(), name='register'),
    path('logout/', LogoutAPIView.as_view(), name='logout'),
    path('me/', CurrentUserAPIView.as_view(), name='me'),
    path(
        'activate/',
        UserProfileActivation.as_view(),
        name='profile_activation',
    ),
    path('token/', include([
        path(
            '',
            DecoratedTokenObtainPairView.as_view(),
            name='token_obtain_pair',
        ),
        path(
            'refresh/',
            DecoratedTokenRefreshView.as_view(),
            name='token_refresh',
        ),
        path(
            'verify/',
            DecoratedTokenVerifyView.as_view(),
            name='token_verify',
        ),
    ])),

    path('password/', include([
        path(
            'change/',
            PasswordChangeAPIView.as_view(),
            name='password_change',
        ),
        path(
            'recover/',
            PasswordRecoverRequestAPIView.as_view(),
            name='password_recover_request',
        ),
        path(
            'reset/',
            PasswordResetAPIView.as_view(),
            name='password_reset',
        ),
    ])),
]
