from drf_yasg.utils import swagger_auto_schema
from rest_framework import permissions, status
from rest_framework.generics import GenericAPIView
from rest_framework.response import Response

from server.apps.auth.logic.serializers import (
    PasswordChangeSerializer,
    PasswordRecoverRequestSerializer,
    PasswordResetSerializer,
)
from server.apps.auth.logic.services import PasswordService
from server.apps.core.mixins import ApiErrorsMixin


class PasswordChangeAPIView(ApiErrorsMixin, GenericAPIView):
    """
    Change user password.

    Take new and old password. If old password is incorrect, throw error, else
    set new password.
    """

    serializer_class = PasswordChangeSerializer
    permission_classes = (permissions.IsAuthenticated,)

    @swagger_auto_schema(
        responses={
            status.HTTP_200_OK: 'New password is set.',
        },
    )
    def post(self, request, *args, **kwargs):
        password_change_serializer = self.get_serializer(data=request.data)
        password_change_serializer.is_valid(raise_exception=True)

        PasswordService().change_password(
            user=self.request.user,
            **password_change_serializer.validated_data,
        )

        return Response(status=status.HTTP_200_OK)


class PasswordRecoverRequestAPIView(ApiErrorsMixin, GenericAPIView):
    """
    Password recover request handler.

    Takes account email and url with password reset token is send to it.
    """

    serializer_class = PasswordRecoverRequestSerializer

    @swagger_auto_schema(
        responses={
            status.HTTP_200_OK: 'Password reset url is sent to email.',
        },
    )
    def post(self, request, *args, **kwargs):
        password_recover_serializer = self.get_serializer(data=request.data)
        password_recover_serializer.is_valid(raise_exception=True)

        PasswordService().password_recover(
            **password_recover_serializer.validated_data,
        )

        return Response(status=status.HTTP_200_OK)


class PasswordResetAPIView(ApiErrorsMixin, GenericAPIView):
    """
    Password reset view.

    Takes password reset token and new password to set. If tokes is valid,
    new password is set for user.
    """

    serializer_class = PasswordResetSerializer

    @swagger_auto_schema(
        responses={
            status.HTTP_200_OK: 'Password reset successfully',
        },
    )
    def post(self, request, *args, **kwargs):
        password_reset_serializer = self.get_serializer(data=request.data)
        password_reset_serializer.is_valid(raise_exception=True)

        PasswordService().password_reset(
            **password_reset_serializer.validated_data,
        )

        return Response(status=status.HTTP_200_OK)
