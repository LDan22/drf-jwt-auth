"""
Extensions of rest_framework_simplejwt.views classes.

Purpose of these views are only to generate right responses in swagger, so they
do not contain additional logic that should be tested, therefore they are
excluded from test coverage (using `pragma: no cover`).
"""

from drf_yasg.utils import swagger_auto_schema
from rest_framework import status
from rest_framework_simplejwt.views import (
    TokenObtainPairView,
    TokenRefreshView,
    TokenVerifyView,
)

from server.apps.auth.logic.serializers import (
    TokenObtainPairResponseSerializer,
    TokenRefreshResponseSerializer,
    TokenVerifyResponseSerializer,
)


class DecoratedTokenObtainPairView(TokenObtainPairView):  # pragma: no cover
    """
    Logins user.

    Takes a set of user credentials and returns an access and refresh JSON web
    token pair to prove the authentication of those credentials.
    """

    @swagger_auto_schema(
        responses={
            status.HTTP_200_OK: TokenObtainPairResponseSerializer(),
            status.HTTP_401_UNAUTHORIZED: 'Token is invalid or expired',
        },
    )
    def post(self, request, *args, **kwargs):
        return super().post(request, *args, **kwargs)


class DecoratedTokenRefreshView(TokenRefreshView):  # pragma: no cover
    """
    Refresh access token.

    Takes a refresh type JSON web token and returns an access type JSON web
    token if the refresh token is valid.
    """

    @swagger_auto_schema(
        responses={
            status.HTTP_200_OK: TokenRefreshResponseSerializer(),
            status.HTTP_401_UNAUTHORIZED: 'Token is invalid or expired',
        },
    )
    def post(self, request, *args, **kwargs):
        return super().post(request, *args, **kwargs)


class DecoratedTokenVerifyView(TokenVerifyView):  # pragma: no cover
    """
    Takes a token and indicates if it is valid.

    This view provides no information about a token's fitness for a particular
    use.
    """

    @swagger_auto_schema(
        responses={
            status.HTTP_200_OK: TokenVerifyResponseSerializer(),
            status.HTTP_401_UNAUTHORIZED: 'Token is invalid or expired',
        },
    )
    def post(self, request, *args, **kwargs):
        return super().post(request, *args, **kwargs)
