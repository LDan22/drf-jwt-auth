from drf_yasg.utils import swagger_auto_schema
from rest_framework import permissions, status
from rest_framework.generics import GenericAPIView
from rest_framework.response import Response
from rest_framework.views import APIView

from server.apps.auth.logic.serializers import (
    AuthenticationResponseSerializer,
    LogoutSerializer,
    RegistrationSerializer,
)
from server.apps.auth.logic.serializers.auth import ProfileActivationSerializer
from server.apps.auth.logic.services import AuthService
from server.apps.core.mixins import ApiErrorsMixin
from server.apps.users.logic.serializers import UserProfileSerializer


class RegistrationAPIView(ApiErrorsMixin, GenericAPIView):
    """
    Registers a new user.

    Takes a set of user credentials and returns data of created user.
    User needs to login after registration if wants to authorize in system.
    """

    serializer_class = RegistrationSerializer

    @swagger_auto_schema(
        responses={
            status.HTTP_201_CREATED: AuthenticationResponseSerializer(),
        },
    )
    def post(self, request, *args, **kwargs):
        register_serializer = self.get_serializer(data=request.data)
        register_serializer.is_valid(raise_exception=True)

        user_with_tokens = AuthService().register(
            **register_serializer.validated_data,
        )

        response_serializer = AuthenticationResponseSerializer(
            user_with_tokens,
            many=False,
        )
        return Response(
            data=response_serializer.data,
            status=status.HTTP_201_CREATED,
        )


class LogoutAPIView(ApiErrorsMixin, GenericAPIView):
    """
    Logout user.

    Take refresh token and blacklist it, if the refresh token is valid.
    """

    serializer_class = LogoutSerializer
    permission_classes = (permissions.IsAuthenticated,)

    @swagger_auto_schema(
        responses={
            status.HTTP_200_OK: 'Logout success.',
        },
    )
    def post(self, request, *args, **kwargs):
        logout_serializer = self.get_serializer(data=request.data)
        logout_serializer.is_valid(raise_exception=True)

        AuthService().blacklist_refresh_token(
            **logout_serializer.validated_data,
        )

        return Response(status=status.HTTP_200_OK)


class UserProfileActivation(ApiErrorsMixin, GenericAPIView):
    """
    User profile activation.

    Take activation_token, validated it and if success activate user account.
    User should be authorized in order to activate profile.
    """

    serializer_class = ProfileActivationSerializer
    permission_classes = (permissions.IsAuthenticated,)

    @swagger_auto_schema(
        responses={
            status.HTTP_200_OK: UserProfileSerializer(),
        },
    )
    def post(self, request, *args, **kwargs):
        activation_serializer = self.get_serializer(data=request.data)
        activation_serializer.is_valid(raise_exception=True)

        user_profile = AuthService().profile_activate(
            request.user,
            **activation_serializer.validated_data,
        )

        response_serializer = UserProfileSerializer(
            user_profile.user,
            many=False,
        )

        return Response(
            data=response_serializer.data,
            status=status.HTTP_200_OK,
        )


class CurrentUserAPIView(APIView):
    """
    Current user.

    Get user and profile data about current authenticated user.
    """

    serializer_class = UserProfileSerializer
    permission_classes = (permissions.IsAuthenticated,)

    @swagger_auto_schema(
        responses={
            status.HTTP_200_OK: UserProfileSerializer(),
        },
    )
    def get(self, request, *args, **kwargs):
        response_serializer = self.serializer_class(request.user, many=False)

        return Response(
            data=response_serializer.data,
            status=status.HTTP_200_OK,
        )
