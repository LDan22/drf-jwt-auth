from server.apps.auth.views.auth import (
    LogoutAPIView,
    RegistrationAPIView,
    UserProfileActivation,
)
from server.apps.auth.views.token import (
    DecoratedTokenObtainPairView,
    DecoratedTokenRefreshView,
    DecoratedTokenVerifyView,
)
