from rest_framework.exceptions import ValidationError

from server.apps.mailings.logic.services import MailingsService
from server.apps.users.logic.selectors import UserSelector, UserTokenSelector
from server.apps.users.logic.services import UserService


class PasswordService(object):
    """Service for managing account password."""

    def __init__(self):
        super().__init__()
        self.user_service = UserService()
        self.user_selector = UserSelector()
        self.user_token_selector = UserTokenSelector()
        self.mailings_service = MailingsService()

    def change_password(self, user, old_password, new_password):
        """Change password for user."""
        if not user.check_password(old_password):
            raise ValidationError(detail='Wrong old password')

        self.user_service.set_password(user, new_password)

    def password_recover(self, email):
        """Check if email is valid and send email with password reset url."""
        user = self.user_selector.get_by_email(email)
        self.mailings_service.send_password_recover_mail(user)

    def password_reset(self, token, new_password):
        """
        Set new password for user, using password reset token.

        Authenticate user using new credentials and return auth response.
        """
        user_token = self.user_token_selector.with_select_related(
            'user',
        ).get_one_by(key=token)
        self.user_service.set_password(user_token.user, new_password)
