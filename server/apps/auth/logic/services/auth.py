from rest_framework_simplejwt.tokens import RefreshToken

from server.apps.mailings.logic.services import MailingsService
from server.apps.users.logic.selectors import UserTokenSelector
from server.apps.users.logic.services import UserService
from server.apps.users.logic.services.user_profile import UserProfileService


class AuthService(object):
    """Service for authentication."""

    def __init__(self):
        super().__init__()
        self.user_service = UserService()
        self.user_profile_service = UserProfileService()
        self.user_token_selector = UserTokenSelector()
        self.mailings_service = MailingsService()

    def register(self, **registration_data):
        """
        Registration logic.

        Send activation mail after all logic is done.
        """
        user = self.user_service.create(**registration_data)
        auth_response = self.authenticate(user)
        self.mailings_service.send_activation_mail(user)
        return auth_response

    def profile_activate(self, user, activation_token):
        user_token = self.user_token_selector.with_select_related(
            'user',
        ).get_one_by(key=activation_token, user=user)
        user_profile = user_token.user.profile
        return self.user_profile_service.set_email_verified(user_profile)

    def authenticate(self, user):
        tokens = self.issue_token_pair(user)
        return {
            'user': user,
            'tokens': tokens,
        }

    @staticmethod
    def issue_token_pair(user):
        refresh = RefreshToken.for_user(user)

        return {
            'refresh': str(refresh),
            'access': str(refresh.access_token),
        }

    @staticmethod
    def blacklist_refresh_token(refresh):
        token = RefreshToken(refresh)
        token.blacklist()
