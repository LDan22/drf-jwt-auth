from rest_framework import serializers


class TokenObtainPairResponseSerializer(serializers.Serializer):
    """Token obtain pair serializer."""

    access = serializers.CharField()
    refresh = serializers.CharField()


class TokenRefreshResponseSerializer(serializers.Serializer):
    """
    Token refresh serializer.

    Used for swagger responses.
    """

    access = serializers.CharField()


class TokenVerifyResponseSerializer(serializers.Serializer):
    """
    Token verify serializer.

    Used for swagger responses.
    """
