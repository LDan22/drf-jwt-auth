from rest_framework import serializers


class PasswordChangeSerializer(serializers.Serializer):
    """Password change serializer."""

    old_password = serializers.CharField()
    new_password = serializers.CharField()


class PasswordRecoverRequestSerializer(serializers.Serializer):
    """Password recover request serializer."""

    email = serializers.EmailField()


class PasswordResetSerializer(serializers.Serializer):
    """Password reset serializer."""

    token = serializers.UUIDField()
    new_password = serializers.CharField()
