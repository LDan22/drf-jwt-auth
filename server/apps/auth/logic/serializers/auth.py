from rest_framework import serializers

from server.apps.auth.logic.serializers.tokens import (
    TokenObtainPairResponseSerializer,
)
from server.apps.users.logic.serializers import UserSerializer


class RegistrationSerializer(serializers.Serializer):
    """
    Registration serializer.

    For registration is enough email and password, but additional information
    could be provided.
    """

    email = serializers.EmailField()
    password = serializers.CharField()
    first_name = serializers.CharField(required=False)
    last_name = serializers.CharField(required=False)


class LogoutSerializer(serializers.Serializer):
    """
    Serializer for logout.

    Need only refresh token for logout.
    """

    refresh = serializers.CharField()


class AuthenticationResponseSerializer(serializers.Serializer):
    """
    Registration response serializer.

    Contains registered user and tokens for authorization.
    """

    user = UserSerializer()
    tokens = TokenObtainPairResponseSerializer()


class ProfileActivationSerializer(serializers.Serializer):
    """Profile activation serializer."""

    activation_token = serializers.UUIDField()
