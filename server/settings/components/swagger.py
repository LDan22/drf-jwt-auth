SWAGGER_SETTINGS = {
    'DEFAULT_PAGINATOR_INSPECTORS': [
        'server.apps.core.swagger.inspectors.ApiLimitOffsetPaginatorInspector',
    ],
    'SECURITY_DEFINITIONS': {
        'Bearer': {
            'type': 'apiKey',
            'name': 'Authorization',
            'in': 'header',
        },
    },
}

REDOC_SETTINGS = {
    'LAZY_RENDERING': False,
}
