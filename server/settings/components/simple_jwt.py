from datetime import timedelta

from server.settings.components import env

# minutes
USER_TOKEN_LIFETIME = env('USER_TOKEN_LIFETIME', cast=int, default=15)

# minutes
ACCESS_TOKEN_LIFETIME = env('ACCESS_TOKEN_LIFETIME', cast=int, default=15)

# days
REFRESH_TOKEN_LIFETIME = env('REFRESH_TOKEN_LIFETIME', cast=int, default=1)

SIMPLE_JWT = {
    'ACCESS_TOKEN_LIFETIME': timedelta(minutes=ACCESS_TOKEN_LIFETIME),
    'REFRESH_TOKEN_LIFETIME': timedelta(days=REFRESH_TOKEN_LIFETIME),
    'ROTATE_REFRESH_TOKENS': False,
    'BLACKLIST_AFTER_ROTATION': True,

    'AUTH_HEADER_TYPES': ('Bearer',),
    'AUTH_HEADER_NAME': 'HTTP_AUTHORIZATION',
}
