import json

import pytest
from rest_framework.reverse import reverse_lazy

register_url = reverse_lazy('auth:register')


@pytest.mark.django_db()
def test_registration_with_valid_data(api_client):
    """Test can register if data is correct."""
    valid_data = {
        'email': 'test@email.com',
        'password': 'test_pass123',
    }
    response = api_client().post(register_url, valid_data)
    response_data = json.loads(response.content)
    assert response.status_code == 201
    assert response_data['user']
    assert response_data['tokens']['access']
    assert response_data['tokens']['refresh']
