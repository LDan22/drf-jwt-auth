import pytest
from rest_framework.reverse import reverse_lazy

password_recover_url = reverse_lazy('auth:password_recover_request')


@pytest.mark.django_db()
def test_password_recover_success(default_user, api_client):
    """Test password recover request is processed successfully."""
    response = api_client().post(password_recover_url, {
        'email': default_user.email,
    })

    assert response.status_code == 200
