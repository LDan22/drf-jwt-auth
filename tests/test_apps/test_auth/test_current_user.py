import json

import pytest
from rest_framework.reverse import reverse_lazy

me_url = reverse_lazy('auth:me')


@pytest.mark.django_db()
def test_can_get_current_user(auth_client, default_user):
    """Test can get data about current authenticated user."""
    response = auth_client.get(me_url)
    assert response.status_code == 200
    response_data = json.loads(response.content)
    assert response_data['id'] == default_user.id
    assert response_data['profile']
