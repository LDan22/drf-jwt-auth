import pytest
from rest_framework.reverse import reverse_lazy
from rest_framework_simplejwt.token_blacklist.models import BlacklistedToken

logout_url = reverse_lazy('auth:logout')


@pytest.mark.django_db()
def test_logout_success(auth_client, auth_tokens):
    """Test can logout with valid refresh token."""
    logout_data = {
        'refresh': str(auth_tokens),
    }
    response = auth_client.post(logout_url, logout_data)

    assert response.status_code == 200
    assert BlacklistedToken.objects.count() == 1
