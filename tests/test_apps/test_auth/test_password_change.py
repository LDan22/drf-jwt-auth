import pytest
from rest_framework.reverse import reverse_lazy

password_change_url = reverse_lazy('auth:password_change')


@pytest.mark.django_db()
def test_password_change_success(django_user_model, default_user, auth_client):
    """Test can change password with valid data."""
    password_change_data = {
        'old_password': 'test_pass123',
        'new_password': 'new_test_pass123',
    }
    response = auth_client.post(password_change_url, password_change_data)
    assert response.status_code == 200
    updated_user = django_user_model.objects.get(id=default_user.id)
    assert updated_user.check_password('new_test_pass123')


@pytest.mark.django_db()
def test_password_change_wrong_old_password(auth_client):
    """Test can change password with valid data."""
    password_change_data = {
        'old_password': 'wrong_test_pass123',
        'new_password': 'new_test_pass123',
    }
    response = auth_client.post(password_change_url, password_change_data)
    assert response.status_code == 400
