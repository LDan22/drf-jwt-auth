import pytest
from django.utils import timezone
from rest_framework.reverse import reverse_lazy

from server.apps.users.logic.selectors.user_profile import UserProfileSelector
from server.apps.users.logic.services.user_token import UserTokenService

activation_url = reverse_lazy('auth:profile_activation')


@pytest.mark.django_db()
def test_profile_activation_success(auth_client, default_user):
    """Test profile is verified success."""
    user_token = UserTokenService().create_account_activation_token(
        default_user,
    )

    response = auth_client.post(activation_url, {
        'activation_token': user_token.key,
    })

    updated_profile = UserProfileSelector().get_one_by(user=default_user)
    assert response.status_code == 200
    assert updated_profile.email_verified


@pytest.mark.django_db()
def test_profile_activation_with_expired_token(auth_client, default_user):
    """Test can not activate if token is expired."""
    user_token = UserTokenService().create_account_activation_token(
        default_user,
    )
    user_token.expires = timezone.now()
    user_token.save()

    response = auth_client.post(activation_url, {
        'activation_token': user_token.key,
    })

    updated_profile = UserProfileSelector().get_one_by(user=default_user)
    assert response.status_code == 400
    assert not updated_profile.email_verified
