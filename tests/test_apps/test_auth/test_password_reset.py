import pytest
from rest_framework.reverse import reverse_lazy

from server.apps.users.logic.services.user_token import UserTokenService

password_reset_url = reverse_lazy('auth:password_reset')


@pytest.mark.django_db()
def test_password_reset_success(default_user, api_client):
    """Test can reset password."""
    user_token = UserTokenService().create_password_recover_token(default_user)

    password_reset_data = {
        'token': user_token.key,
        'new_password': 'new_test_pass123',
    }

    response = api_client().post(password_reset_url, password_reset_data)

    assert response.status_code == 200
