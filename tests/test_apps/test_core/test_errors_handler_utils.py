from server.apps.core.mixins import get_error_message, unpack_single_key


def test_get_error_message_with_message_dict():
    """Test dict with message is returned when present."""

    class Exc(object):
        message_dict = {
            'error': 'Error',
        }

    exc = Exc()

    error_message = get_error_message(exc)

    assert error_message == exc.message_dict


def test_get_error_message():
    """Test only message is returned if dict not provided."""

    class Exc(object):
        message = 'Error'

    exc = Exc()

    error_message = get_error_message(exc)

    assert error_message == exc.message


def test_get_error_message_with_list():
    """Test list of errors is split."""

    class Exc(object):
        message = ['Error1', 'Error2']

    exc = Exc()

    error_message = get_error_message(exc)

    assert error_message == 'Error1, Error2'


def test_get_error_message_no_message():
    """Test default handling."""

    class Exc(object):
        error = 'Error'

    exc = Exc()

    error_message = get_error_message(exc)

    assert error_message == str(exc)


def test_unpack_single_key():
    """Test single key is not separated."""
    unpacked = unpack_single_key('key', 'value')
    assert list(unpacked) == [('key', 'value')]
