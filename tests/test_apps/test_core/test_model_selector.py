import pytest

mock_users = 'users'


@pytest.mark.usefixtures(mock_users)
@pytest.mark.django_db()
def test_get_all_success(user_selector):
    """Test can get all instances."""
    users = user_selector.get_all()
    assert len(users) == 5


@pytest.mark.usefixtures(mock_users)
@pytest.mark.django_db()
def test_get_by_id_success(user_selector):
    """Test can get instance by its id."""
    user = user_selector.get_by_id(3)
    assert user


@pytest.mark.usefixtures(mock_users)
@pytest.mark.django_db()
def test_get_by_id_with_prefetch_related(user_selector):
    """Test can get with prefetch related."""
    user = user_selector.with_prefetch_related('groups').get_by_id(3)
    assert user


@pytest.mark.django_db()
def test_get_by_id_with_select_related(permission_selector):
    """Test can get with select related."""
    permission = permission_selector.with_select_related(
        'content_type',
    ).get_by_id(1)

    assert permission


@pytest.mark.usefixtures(mock_users)
@pytest.mark.django_db()
def test_get_by_field(user_selector):
    """Test can get by one or multiple fields."""
    user = user_selector.get_one_by(id=1)
    assert user


@pytest.mark.usefixtures(mock_users)
@pytest.mark.django_db()
def test_filter_by(user_selector):
    """Test can filter by one or multiple fields."""
    users = user_selector.filter_by(id__gte=1, id__lte=3)
    assert len(users) == 3
