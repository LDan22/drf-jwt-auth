import pytest
from mixer.backend.django import mixer

from server.apps.users.models import UserToken


@pytest.mark.django_db()
def test_user_token_model():
    """Test instance has correct representation."""
    user_token = mixer.blend(UserToken)
    assert str(user_token) == str(user_token.key)
