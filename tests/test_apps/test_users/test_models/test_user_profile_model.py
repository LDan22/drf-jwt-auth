import tempfile

import pytest
from django.core.files.uploadedfile import SimpleUploadedFile
from mixer.backend.django import mixer

from server.apps.users.models import UserProfile
from tests.test_apps.test_core.conftest import UserService


@pytest.mark.django_db()
def test_user_profile_model():
    """Test instance has correct representation."""
    instance = mixer.blend(UserProfile)
    assert str(instance) == instance.user.email


@pytest.mark.django_db()
def test_profile_pic_upload_path(django_user_model):
    """Test upload path of profile picture is correct."""
    user = UserService().create(email='test@email.com', password='bad_pass')
    image = tempfile.NamedTemporaryFile(suffix='.png')
    with open(image.name, 'rb') as image_bytes:
        profile_pic = SimpleUploadedFile(
            name=image.name,
            content=image_bytes.read(),
            content_type='image/png',
        )
    profile = UserProfile.objects.create(
        user=user,
        profile_pic=profile_pic,
    )
    assert str(profile.profile_pic) == 'profile_pics/{0}/{1}'.format(
        user.email,
        profile_pic.name,
    )
