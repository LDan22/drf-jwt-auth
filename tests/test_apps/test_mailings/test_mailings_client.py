import pytest

from server.apps.mailings.logic.clients import MailingsClient
from server.apps.mailings.logic.mails import BaseMail


def test_mailings_client_without_impl_methods():
    """Test it raises exception if using client without implementing methods."""
    client = MailingsClient(
        to_emails=['to@email.com'],
        mail=BaseMail('Test'),
    )
    with pytest.raises(ValueError, match='Provider class can not be None.'):
        client.get_provider()

    with pytest.raises(ValueError, match='Formatter class can not be None.'):
        client.get_formatter()
