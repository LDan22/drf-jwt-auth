import pytest

from server.apps.mailings.logic.mailers import BaseMailer
from server.apps.mailings.logic.mails import BaseMail


def test_base_mailer_without_implemented_methods():
    """Test it raises exception if not implementing required methods."""
    mailer = BaseMailer(
        to_emails=['to@email.com'],
        mail=BaseMail('Test'),
    )
    with pytest.raises(ValueError, match='Client class can not be None'):
        mailer.send()
