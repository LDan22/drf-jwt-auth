import pytest
from mixer.backend.django import mixer
from rest_framework.response import Response
from rest_framework.test import APIClient
from rest_framework_simplejwt.tokens import RefreshToken

from server.apps.users.logic.services.user_profile import UserProfileService


@pytest.fixture()
def default_user(django_user_model):
    user = django_user_model.objects.create_user(
        email='test@email.com',
        password='test_pass123',
    )
    UserProfileService().create(user=user)
    return user


@pytest.fixture()
def auth_tokens(default_user):
    """Refresh and access tokens for authentication."""
    return RefreshToken.for_user(default_user)


@pytest.fixture()
def auth_client(auth_tokens):
    """Api client with valid authorization header."""
    client = APIClient()
    client.credentials(
        HTTP_AUTHORIZATION='Bearer {0}'.format(str(auth_tokens.access_token)),
    )

    return client


def mock_send(self, email):
    """
    Mock method for sending emails with SendGridAPIClient.

    It is a method, so it requires `self` in its signature.
    """
    return Response(status=200)


@pytest.fixture(scope='session', autouse=True)
def _mock_sendgrid_api(session_mocker):
    """Mock SendGridAPIClient for all tests."""
    session_mocker.patch(
        'sendgrid.SendGridAPIClient.send',
        mock_send,
    )


@pytest.fixture()
def users(django_user_model):
    """Mock list of users."""
    user_ids = (pk + 1 for pk in range(5))
    return mixer.cycle(5).blend(django_user_model, id=user_ids)
